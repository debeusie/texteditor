#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFile>
#include <QFileDialog>
#include <QTextStream>
#include <QMessageBox>
#include <QPrinter>
#include <QPrintDialog>

#include <QDesktopServices>


#include <QtGui>
#include <QWidget>
#include <QHBoxLayout>


#include <QSettings>
#include <QMimeData>
#include <QByteArray>
#include <stdlib.h>
#include <QTextDocument>


#include <QPointer>

#include <QAction>
#include <QApplication>
#include <QClipboard>
#include <QFileInfo>
#include <QMenu>
#include <QMenuBar>
#include <QTextEdit>
#include <QStatusBar>
#include <QToolBar>
#include <QTextCursor>
#include <QTextDocumentWriter>
#include <QTextList>
#include <QFont>



QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }


class QAction;
class QTextEdit;
class QTextCharFormat;
class QMenu;
class QPrinter;

QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private slots:
    void on_actionNew_triggered();

    void on_actionOpen_triggered();

    void on_actionSave_as_triggered();

    void on_actionPrint_triggered();

    void on_actionExit_triggered();

    void on_actionCopy_triggered();

    void on_actionPaste_triggered();

    void on_actionCut_triggered();

    void on_actionUndo_triggered();

    void on_actionRedo_triggered();

     void on_actionBold_triggered();

     void on_actionNormal_text_triggered();

     void on_actionItalic_triggered();

     void on_actionUnderline_triggered();

     void on_actionZoom_in_triggered();

     void on_actionZoom_out_triggered();

     void on_actionAbout_triggered();

     void on_actionHelp_triggered();

     void on_findButton_clicked();

private:
    Ui::MainWindow *ui;
    QString currentFile = "";



};
#endif // MAINWINDOW_H
