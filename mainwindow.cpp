#include "mainwindow.h"
#include "ui_mainwindow.h"


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->verticalLayout_2);


    this->setWindowTitle(QApplication::translate("windowlayout", "Text editor"));




    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);

}

MainWindow::~MainWindow()
{
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
    delete ui;
}



//New/Nový
void MainWindow::on_actionNew_triggered()
{
    currentFile.clear();
    ui->textEdit->setText(QString());

}

//Open/Otevřít
void MainWindow::on_actionOpen_triggered()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Open the file");
    QFile file(fileName);
    currentFile = fileName;
    if(!file.open(QIODevice::ReadOnly | QFile::Text)){
        QMessageBox::warning(this, "Warning", "Can't open file: " + file.errorString());
    }
    setWindowTitle(fileName);
    QTextStream in(&file);
    QString text = in.readAll();
    file.close();
    ui->textEdit->setFontWeight(QFont::Normal);
    bool italic=false;
    ui->textEdit->setFontItalic(italic);
    bool underline=false;
    ui->textEdit->setFontUnderline(underline);
    if(fileName.endsWith(".html")){
        ui->textEdit ->setHtml(text);
    }else{
        ui->textEdit ->setText(text);
    }

    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);


}

//Save as/Uložit jako
void MainWindow::on_actionSave_as_triggered()
{
    QString fileName = QFileDialog::getSaveFileName(this, "Save as", ".", "Text (*.txt);; HTML (*.html)");
    QFile file(fileName);
    if(!file.open(QFile::WriteOnly | QFile::Text)){
        QMessageBox::warning(this, "Warning", "Can't save file: " + file.errorString());
        return;
    }
    currentFile = fileName;
    setWindowTitle(fileName);
    QTextStream out(&file);
    QString text;
    if(fileName.endsWith(".html")){
        text = ui->textEdit->toHtml();
    }else{
        text = ui->textEdit->toPlainText();
    }
    out << text;
    file.close();

}

//Print/Tisk
void MainWindow::on_actionPrint_triggered()
{
    QPrinter printer;
    printer.setPrinterName("Printer name");
    QPrintDialog pDialog(&printer, this);
    if(pDialog.exec() == QDialog::Rejected){
        QMessageBox::warning(this, "Warning", "Can't access printer");
        return;
    }
    ui->textEdit->print(&printer);

}

//Exit/Zavřít
void MainWindow::on_actionExit_triggered()
{
    QApplication::quit();
}

//Copy/Kopírovat
void MainWindow::on_actionCopy_triggered()
{
    ui->textEdit->copy();
}

//Paste/Vložit
void MainWindow::on_actionPaste_triggered()
{
    ui->textEdit->paste();
}

//Cut/Vyjmout
void MainWindow::on_actionCut_triggered()
{
    ui->textEdit->cut();
}

//Undo/Vrátit dozadu <-
void MainWindow::on_actionUndo_triggered()
{
     ui->textEdit->undo();
}

//Redo//Vratit dopředu ->
void MainWindow::on_actionRedo_triggered()
{
    ui->textEdit->redo();
}
//Normal text/normální text/základní text
void MainWindow::on_actionNormal_text_triggered()
{
    ui->textEdit->setFontWeight(QFont::Normal);
    bool italic=false;
    ui->textEdit->setFontItalic(italic);
    bool underline=false;
    ui->textEdit->setFontUnderline(underline);
}

//Bold/Tučně
void MainWindow::on_actionBold_triggered()
{

    ui->textEdit->setFontWeight(QFont::Bold);

}

//Italic/Kurzíva
void MainWindow::on_actionItalic_triggered()
{
    bool italic=true;
    ui->textEdit->setFontItalic(italic);
}

//Underline/Podtržení
void MainWindow::on_actionUnderline_triggered()
{
    bool underline=true;
    ui->textEdit->setFontUnderline(underline);
}

//Zoom in/Přiblížit
void MainWindow::on_actionZoom_in_triggered()
{
    ui->textEdit->zoomIn();
}

//Zoom out/Oddálit
void MainWindow::on_actionZoom_out_triggered()
{    
    ui->textEdit->zoomOut();
}


//About/o programu
void MainWindow::on_actionAbout_triggered()
{
    QMessageBox::about(this, tr("About"), tr("This example demonstrates my term project 'text editor'"));
}

//Help/nápověda
void MainWindow::on_actionHelp_triggered()
{
    QFile::copy(":/Obrazky/help.pdf", "help.pdf");
    QDesktopServices :: openUrl( QUrl ( "help.pdf" , QUrl :: TolerantMode));
}



//Find/hledat
void MainWindow::on_findButton_clicked()
{
    QString searchString = ui->lineEdit->text();
    ui->textEdit->find(searchString, QTextDocument::FindWholeWords);
}


